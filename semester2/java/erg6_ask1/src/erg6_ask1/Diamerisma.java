/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package erg6_ask1;

/**
 *
 * @author user
 */
public class Diamerisma extends Katoikia {
    
    private double TimiD;

    public Diamerisma(double TimiD, int code, int rooms, double sqm, String adress) {
        super(code, rooms, sqm, adress);
        this.TimiD = TimiD;
    }

    @Override
    public void Poso(){
        if (this.getCode()==1){
            System.out.println("Kostos spitiou : " + (TimiD + 1200.00));
        }else{
            System.out.println("Kostos spitiou : " + (TimiD*1.5));
        }
        
    }

    @Override
    public String typosSynalagis() {
        return super.typosSynalagis();
    }
 
}
