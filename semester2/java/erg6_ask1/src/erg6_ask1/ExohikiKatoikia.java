/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package erg6_ask1;

/**
 *
 * @author user
 */
public class ExohikiKatoikia extends Katoikia{
    
    private double timiEx;

    public ExohikiKatoikia(double timiEx, int code, int rooms, double sqm, String adress) {
        super(code, rooms, sqm, adress);
        this.timiEx = timiEx;
    }
    
    
    @Override
    public void Poso(){
        if (this.getCode()==1){
            System.out.println("Kostos spitiou : " + (timiEx + 1200.00));
        }else{
            System.out.println("Kostos spitiou : " + (timiEx*1.5));
        }
        
    }

    @Override
    public String typosSynalagis() {
        return super.typosSynalagis();
    }    
    
}
