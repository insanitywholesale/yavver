package erg6_ask1;

public class Katoikia {

	private int code;
	private int rooms;
	private double sqm;
	private String adress;

	public Katoikia(int code, int rooms, double sqm, String adress) {
		this.code = code;
		this.rooms = rooms;
		this.sqm = sqm;
		this.adress = adress;
	}

	public int getCode() {
		return code;
	}

	public int getRooms() {
		return rooms;
	}

	public double getSqm() {
		return sqm;
	}

	public String getAdress() {
		return adress;
	}

	public String typosSynalagis() {
		switch (this.code) {
			case 1:
				return "Polisi";
			case 2:
				return "Enoikiasi";
			default:
				return "kati phge lathos me ton typo synallaghs";
		}
	}

	public void Poso() {
		switch (this.code) {
			case 1:
				System.out.println("Polisi + 1200");
				break;
			case 2:
				System.out.println("Enoikio * 1.5");
				break;
			default:
				System.out.println("kati phge lathos me to poso");
				break;
		}
	}

	@Override
	public String toString() {
		return "Katoikia {" + "code = " + this.code + ", rooms = " + this.rooms + ", sqm = " + this.sqm + ", adress = " + this.adress + "}";
	}

}
