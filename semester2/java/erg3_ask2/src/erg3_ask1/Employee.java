package erg3_ask1;

public class Employee {
    
    private String onoma;
    private String epwnymo;
    private long AFM;
    private double misthos;

    Employee(String on, String ep, long afm, double mi) {
        onoma = on;
        epwnymo = ep;
        AFM = afm;
        misthos = mi;
    }

    String getOnoma(){return onoma;}
    String getEpwnymo(){return epwnymo;}
    long getAFM(){return AFM;}
    double getMisthos(){return misthos;}

    @Override
    public String toString() {
        return "Employee{" + "onoma= " + onoma + ", epwnymo= " + epwnymo + ", AFM= " + AFM + ", misthos= " + misthos + '}';
    }

}
