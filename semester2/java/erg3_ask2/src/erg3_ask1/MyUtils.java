package erg3_ask1;

public class MyUtils {
    
    public static int anazitisiMeEpwnymo(Employee pinYp[], String s){
        int i;
        int result = -1;
        for(i=0;i<pinYp.length;i++){
            if(s.equals(pinYp[i].getEpwnymo())){
                result = i;
            }
        }
        return result;
    }

    public static int anazitisiMeAFM(Employee pinYp[], long afm){
        int i;
        int left = 0, right = pinYp.length - 1, mid;
        int result = -1;

        while (result == -1 && left <= right) {
            mid = (left + right) / 2;
            if(afm < pinYp[mid].getAFM()){
                right = mid - 1;
            }
            else if(afm > pinYp[mid].getAFM()) {
                left = mid + 1;
            }
            else {
                result = mid;
            }
        }
        return result;
    }

    
}
