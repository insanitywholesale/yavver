package erg3_ask1;

public class Erg3_ask2 {

    public static void main(String[] args) {
        int N;
        N = 5;
        Employee pinYp[] = new Employee[N];

        pinYp[0] = new Employee("BB", "hh", 0001, 5000.0);
        pinYp[1] = new Employee("NN", "gg", 0002, 3000.0);
        pinYp[2] = new Employee("CC", "ee", 0003, 500.0);
        pinYp[3] = new Employee("ZZ", "vv", 0004, 9000.0);
        pinYp[4] = new Employee("AA", "cc", 0005, 14000.0);

        String s = "vv";
        long l = 0005;

        int searchEpwnymo = MyUtils.anazitisiMeEpwnymo(pinYp, s);
        int searchAFM = MyUtils.anazitisiMeAFM(pinYp, l);
        
        if(searchEpwnymo == -1) {
            System.out.println("no employee with that surname");
        }
        else {
            System.out.println("anazitisi me afm: \nthe employee with surname: " + s + "\n  " + pinYp[searchEpwnymo] + "\nis in the following position in the table: " + searchEpwnymo);
        }
        
        if(searchAFM == -1) {
            System.out.println("no employee with that AFM");
        }
        else {
            System.out.println("anazitisi me afm: \nthe employee with AFM: " + l + " \n  " + pinYp[searchAFM] + "\nis in the following position in the table: " + searchAFM);
        }

    }

    
}
