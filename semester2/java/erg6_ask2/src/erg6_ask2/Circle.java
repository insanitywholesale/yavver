package erg6_ask2;

public class Circle extends Shape {
	private double R;

	public Circle(double R) {
		this.R = R;
	}
	
	@Override
	public double area(){
		return 2 * R * Math.PI;
	}
	
	@Override
	public String toString() {
		return "this is a circle and its area is: " + String.valueOf(area());
	}
}
