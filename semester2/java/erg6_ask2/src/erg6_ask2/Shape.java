/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package erg6_ask2;

/**
 *
 * @author angle
 */
public abstract class Shape {
	private double X;
	private double Y;
	
	public double getX() {
		return X;
	}

	public double getY() {
		return Y;
	}
	
	public abstract double area();
	
	@Override
	public abstract String toString();

	
}
