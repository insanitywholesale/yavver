package erg6_ask2;

public class Erg6_ask2 {

	public static void main(String[] args) {
		Shape pin[] = new Shape[4];
		pin[0] = new Circle(12.0);
		pin[1] = new Rectangle(19.5, 13.0);
		pin[2] = new Circle(8.04);
		pin[3] = new Rectangle(39.1, 1.08);
		
		for (Shape pin1 : pin) {
			System.out.println(pin1);
		}
		
	}
	
}
