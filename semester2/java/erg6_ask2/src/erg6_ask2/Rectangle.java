package erg6_ask2;

public class Rectangle extends Shape {
	private double X;
	private double Y;

	public Rectangle(double X, double Y) {
		this.X = X;
		this.Y = Y;
	}
	
	@Override
	public double area(){
		return X * Y;
	}
	
	@Override
	public String toString() {
		return "this is a rectangle and its area is: " + String.valueOf(area());
	}
	
}
