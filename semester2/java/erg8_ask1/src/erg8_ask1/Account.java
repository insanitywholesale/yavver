package erg8_ask1;

public class Account {
	String epwnymia;
	double balance;
	double posoKinisis;

	public Account(String epwnymia, double balance, double posoKinisis) {
		this.epwnymia = epwnymia;
		this.balance = balance;
		this.posoKinisis = posoKinisis;
	}

	public String getEpwnymia() {
		return epwnymia;
	}

	public double getBalance() {
		return balance;
	}

	public double getPosoKinisis() {
		return posoKinisis;
	}

	@Override
	public String toString() {
		return "Account{" + "epwnymia=" + epwnymia + ", balance=" + balance + ", posoKinisis=" + posoKinisis + '}';
	}
	
}

interface iDebit {
	void debit(String name, double balance, double poso);
}

interface iCredit {
	void credit(String name, double balance, double poso);
}

interface iMetafora {
	void metafora(String name, double balance, double poso);
}