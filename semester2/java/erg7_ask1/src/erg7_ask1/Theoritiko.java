package erg7_ask1;

public class Theoritiko extends Course {
    private double vathmosEksetasisTheorias;
    private double vathmosErgasias;

    public Theoritiko(double vathmosEksetasisTheorias, double vathmosErgasias) {
        this.vathmosEksetasisTheorias = vathmosEksetasisTheorias;
        this.vathmosErgasias = vathmosErgasias;
    }
    
    @Override
    public double TelikosVathmos() {
        return 0.7*vathmosEksetasisTheorias + 0.3*vathmosErgasias;
    }
    
    @Override
    public String toString() {
        String s = "vathmos theorias: " + vathmosEksetasisTheorias + "\n";
        s += "vathmos ergasias: " + vathmosErgasias + "\n";
        s += "telikos vathmos: " + TelikosVathmos();
        return s;
    }
    
}
