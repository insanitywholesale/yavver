package erg7_ask1;

public class Erg7_ask1 {

    public static void main(String[] args) {
        Course pin[] = new Course[3];
        pin[0] = new Meikto(6.4, 7.1);
        pin[1] = new Ergasthriako(8.2);
        pin[2] = new Theoritiko(7.9, 8.3);
        
        for(int i=0;i<pin.length;i++) {
            System.out.println("mathima " + String.valueOf(i+1));
            System.out.println(pin[i] + "\n");
        }
        
    }
    
}
