package erg7_ask2;

public class MeiktoMeErgasia extends Course {
    private double vathmosEksetasisTheorias;
    private double vathmosEksetasisErgasthriou;
    private double vathmosErgasias;
    
    public MeiktoMeErgasia(double vathmosEksetasisTheorias, double vathmosEksetasisErgasthriou, double vathmosErgasias) {
        this.vathmosEksetasisTheorias = vathmosEksetasisTheorias;
        this.vathmosEksetasisErgasthriou = vathmosEksetasisErgasthriou;
        this.vathmosErgasias = vathmosErgasias;
    }
    
    @Override
    public double TelikosVathmos() {
        return 0.4*vathmosEksetasisTheorias + 0.4*vathmosEksetasisTheorias + 0.2*vathmosErgasias;
    }
    
    @Override
    public String toString() {
        String s = "vathmos theorias: " + vathmosEksetasisTheorias + "\n";
        s += "vathmos ergasthriou: " + vathmosEksetasisErgasthriou + "\n";
        s += "vathmos ergasias: " + vathmosErgasias + "\n";
        s += "telikos vathmos: " + TelikosVathmos();
        return s;
    }
}
