package erg7_ask2;

public class Ergasthriako extends Course {
    private double vathmosEksetasisErgasthriou;

    public Ergasthriako(double vathmosEksetasisErgasthriou) {
        this.vathmosEksetasisErgasthriou = vathmosEksetasisErgasthriou;
    }
    
    @Override
    public double TelikosVathmos(){
        return vathmosEksetasisErgasthriou;
    }
    
    @Override
    public String toString() {
        String s = "vathmos ergasthriou: " + vathmosEksetasisErgasthriou + "\n";;
        s += "telikos vathmos: " + TelikosVathmos();
        return s;
    }
    
}
