package erg7_ask2;

public class Meikto extends Course {
    private double vathmosEksetasisTheorias;
    private double vathmosEksetasisErgasthriou;

    public Meikto(double vathmosEksetasisTheorias, double vathmosEksetasisErgasthriou) {
        this.vathmosEksetasisTheorias = vathmosEksetasisTheorias;
        this.vathmosEksetasisErgasthriou = vathmosEksetasisErgasthriou;
    }
    
    @Override
    public double TelikosVathmos() {
        return 0.6*vathmosEksetasisTheorias + 0.4*vathmosEksetasisTheorias;
    }
    
    @Override
    public String toString() {
        String s = "vathmos theorias: " + vathmosEksetasisTheorias + "\n";
        s += "vathmos ergasthriou: " + vathmosEksetasisErgasthriou + "\n";
        s += "telikos vathmos: " + TelikosVathmos();
        return s;
    }
    
}
