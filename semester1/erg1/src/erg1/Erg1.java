/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package erg1;

/**
 *
 * @author angle
 */
public class Erg1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int num;
        int sum = 0;
        int count = 0;
        
        num = (int)(Math.random()*10);
        System.out.println("The number is : " + num);
        
        while (num > 0) {
            sum += num;
            count ++;
            num = (int)(Math.random()*10);
            System.out.println("The number is : " + num);

        }
    
        System.out.println(count + " non-zero numbers were created and their sum is " + sum);
        
    }
    
}
